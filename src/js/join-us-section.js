import { validate } from "./email-validator.js";

class Section {
  constructor(element) {
    this.element = element;
  }

  remove() {
    this.element.remove();
  }

  handleFormSubmit(event, input, button) {
    event.preventDefault();
    const email = input.value;
    console.log(email);
    if (
      validate(email) &&
      button.classList.contains("app-section__button--unsubscribe")
    ) {
      this.unsubscribe(input, button);
    } else if (validate(email)) {
      this.subscribe(email, input, button);
    } else {
      alert("Email is not valid!");
      localStorage.removeItem("subscriptionEmail");
      input.value = "";
    }
  }

  addFormSubmitListener(form, input, button) {
    const storedEmail = localStorage.getItem("subscriptionEmail");

    if (storedEmail) {
      input.value = storedEmail;
    }

    form.addEventListener("submit", (event) => {
      this.handleFormSubmit(event, input, button);
    });
  }

  subscribe(email, input, button) {
    alert("Email is valid!");
    localStorage.setItem("subscriptionEmail", email);
    input.style.display = "none";
    button.classList.remove("app-section__button--subscribe");
    button.classList.add("app-section__button--unsubscribe");
    button.textContent = "Unsubscribe";
  }

  unsubscribe(input, button) {
    localStorage.removeItem("subscriptionEmail");
    input.style.display = "inline-block";
    button.classList.remove("app-section__button--unsubscribe");
    button.classList.add("app-section__button--subscribe");
    button.textContent = "Subscribe";
    input.value = "";
  }
}
export class StandardSection extends Section {
  constructor() {
    super(document.createElement("section"));
    this.element.classList.add("app-section", "app-section__join-program");

    const title = document.createElement("h1");
    title.classList.add("app-title");
    title.textContent = "Join Our Program";

    const subtitle = document.createElement("h2");
    subtitle.classList.add("app-subtitle");
    subtitle.innerHTML =
      "Sed do eiusmod tempor incididunt <br /> ut labore et dolore magna aliqua.";

    const form = document.createElement("form");

    const input = document.createElement("input");
    input.setAttribute("type", "text");
    input.setAttribute("placeholder", "Email");
    form.appendChild(input);

    const button = document.createElement("button");
    button.setAttribute("type", "submit");
    button.classList.add(
      "app-section__button", // eslint-disable-next-line prettier/prettier
      "app-section__button--subscribe" 
    );
    button.textContent = "Subscribe";
    form.appendChild(button);

    const inputGroup = document.createElement("div");
    inputGroup.classList.add("input-group");
    inputGroup.appendChild(form);

    this.element.appendChild(title);
    this.element.appendChild(subtitle);
    this.element.appendChild(inputGroup);

    const footer = document.querySelector(".app-footer");
    footer.parentNode.insertBefore(this.element, footer);

    this.addFormSubmitListener(form, input, button);
  }
}

export class AdvancedSection extends Section {
  constructor() {
    super(document.createElement("section"));
    this.element.classList.add("app-section", "app-section__join-program");

    const title = document.createElement("h1");
    title.classList.add("app-title");
    title.textContent = "Join Our Advanced Program";

    const subtitle = document.createElement("h2");
    subtitle.classList.add("app-subtitle");
    subtitle.innerHTML =
      "Sed do eiusmod tempor incididunt <br /> ut labore et dolore magna aliqua.";

    const form = document.createElement("form");

    const input = document.createElement("input");
    input.setAttribute("type", "text");
    input.setAttribute("placeholder", "Email");
    form.appendChild(input);

    const button = document.createElement("button");
    button.setAttribute("type", "submit");
    button.classList.add(
      "app-section__button",
      "app-section__button--advanced"
    );
    button.textContent = "Subscribe to Advanced Program";
    form.appendChild(button);

    const inputGroup = document.createElement("div");
    inputGroup.classList.add("input-group");
    inputGroup.appendChild(form);

    this.element.appendChild(title);
    this.element.appendChild(subtitle);
    this.element.appendChild(inputGroup);

    const footer = document.querySelector(".app-footer");
    footer.parentNode.insertBefore(this.element, footer);

    this.addFormSubmitListener(form, input, button);
  }
}

export class SectionCreator {
  static create(type) {
    if (type === "standard") {
      return new StandardSection();
    } else if (type === "advanced") {
      return new AdvancedSection();
    } else {
      throw new Error("Invalid section type");
    }
  }
}
