import "../styles/style.css";

import { SectionCreator } from "./join-us-section.js";

document.addEventListener("DOMContentLoaded", function () {
  function createStandardSection() {
    const standardSection = SectionCreator.create("standard");

    standardSection.element
      .querySelector(".app-section__join-program form")
      .addEventListener("submit", function (event) {
        event.preventDefault();
      });
  }

  createStandardSection();
});
